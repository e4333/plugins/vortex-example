package fr.evenus.vortex_example;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.WorldMock;
import fr.evenus.vortex_example.team.Team;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConfigTest {
  private Config config;
  private Config emptyConfig;
  private WorldMock world;
  private List<LogRecord> logs;
  private Logger logger;

  @BeforeEach
  void setUp() throws InvalidConfigurationException {
    ServerMock server = MockBukkit.mock();
    logger = Logger.getAnonymousLogger();
    logs = new LinkedList<>();
    Handler handler = new Handler() {
      @Override
      public void publish(LogRecord logRecord) {
        logs.add(logRecord);
      }

      @Override
      public void flush() {}

      @Override
      public void close() throws SecurityException {}
    };
    logger.addHandler(handler);
    emptyConfig = new Config(new YamlConfiguration(), logger);
    YamlConfiguration tmp = new YamlConfiguration();
    tmp.loadFromString("""
            teams:
              - color: red
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
    config = new Config(tmp, logger);
    world = server.addSimpleWorld("world");
  }

  @AfterEach
  void tearDown() {
    MockBukkit.unmock();
  }

  @Test
  void getTeams() {
    Assertions.assertEquals(Collections.emptyList(), emptyConfig.getTeams());
    LinkedList<Team> teams = new LinkedList<>();
    teams.add(new Team(NamedTextColor.RED, new Location(world, 0, 0, 0)));
    Assertions.assertIterableEquals(teams, config.getTeams());
  }

  @Test
  void getTeamsWrongSpawnpoint() throws InvalidConfigurationException {
    YamlConfiguration tmp = new YamlConfiguration();
    tmp.loadFromString("""
            teams:
              - color: red
                spawnpoint: []
            """);
    Config config = new Config(tmp, logger);
    LinkedList<Team> teams =  new LinkedList<>();
    Assertions.assertIterableEquals(teams, config.getTeams());
    List<LogRecord> trueLogs = new LinkedList<>();
    trueLogs.add(new LogRecord(Level.WARNING, "The spawnpoint of team red is not of proper shape : []"));
    trueLogs.add(new LogRecord(Level.WARNING, "java.lang.ClassCastException: class java.util.ArrayList cannot be cast to class java.util.HashMap (java.util.ArrayList and java.util.HashMap are in module java.base of loader 'bootstrap')"));
    Assertions.assertEquals(trueLogs.size(), logs.size(), "The two logs have not the same length : " + trueLogs.size() + " expected, " + logs.size() + " received.");
    Iterator<LogRecord> trueLogsIterator = trueLogs.iterator();
    for (LogRecord record : logs){
      LogRecord trueRecord = trueLogsIterator.next();
      Assertions.assertEquals(trueRecord.getLevel(), record.getLevel());
      Assertions.assertEquals(trueRecord.getMessage(), record.getMessage());
    }
  }

  @Test
  void getTeamsWrongColor() throws InvalidConfigurationException {
    YamlConfiguration tmp = new YamlConfiguration();
    tmp.loadFromString("""
            teams:
              - color: read
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
    Config config = new Config(tmp, logger);
    LinkedList<Team> teams =  new LinkedList<>();
    Assertions.assertIterableEquals(teams, config.getTeams());
    List<LogRecord> trueLogs = new LinkedList<>();
    trueLogs.add(new LogRecord(Level.WARNING, "The color read is not a valid color."));
    Assertions.assertEquals(trueLogs.size(), logs.size(), "The two logs have not the same length : " + trueLogs.size() + " expected, " + logs.size() + " received.");
    Iterator<LogRecord> trueLogsIterator = trueLogs.iterator();
    for (LogRecord record : logs){
      LogRecord trueRecord = trueLogsIterator.next();
      Assertions.assertEquals(trueRecord.getLevel(), record.getLevel());
      Assertions.assertEquals(trueRecord.getMessage(), record.getMessage());
    }
  }
  @Test
  void getTeamsWrongShape() throws InvalidConfigurationException {
    YamlConfiguration tmp = new YamlConfiguration();
    tmp.loadFromString("""
            teams:
              - color:
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
    Config config = new Config(tmp, logger);
    LinkedList<Team> teams =  new LinkedList<>();
    Assertions.assertIterableEquals(teams, config.getTeams());
    List<LogRecord> trueLogs = new LinkedList<>();
    trueLogs.add(new LogRecord(Level.WARNING, "A team in the config files is not of the expected format : {color=null, spawnpoint={x=0, y=0, z=0, world=world}}"));
    trueLogs.add(new LogRecord(Level.WARNING, "java.lang.NullPointerException: Cannot invoke \"Object.toString()\" because the return value of \"java.util.HashMap.get(Object)\" is null"));
    Assertions.assertEquals(trueLogs.size(), logs.size(), "The two logs have not the same length : " + trueLogs.size() + " expected, " + logs.size() + " received.");
    Iterator<LogRecord> trueLogsIterator = trueLogs.iterator();
    for (LogRecord record : logs){
      LogRecord trueRecord = trueLogsIterator.next();
      Assertions.assertEquals(trueRecord.getLevel(), record.getLevel());
      Assertions.assertEquals(trueRecord.getMessage(), record.getMessage());
    }
  }
}
