/*
 * @Author: Ultraxime
 * @Date:   2023-11-11 19:15:02
 * @Last Modified by:   Ultraxime
 * @Last Modified time: 2023-11-11 19:16:15
 */

// This file is part of Vortex Example.
//
// Vortex Example is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Vortex Example is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vortex Example. If not, see <https://www.gnu.org/licenses/>.

package fr.evenus.vortex_example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MainTest {
  @Test
  void onEnable() {}

  @Test
  void getConfig() {}
}
