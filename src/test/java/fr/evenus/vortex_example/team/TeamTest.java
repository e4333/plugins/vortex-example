package fr.evenus.vortex_example.team;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.WorldMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import java.util.HashSet;
import java.util.Iterator;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TeamTest {
  private Team team;
  private PlayerMock player;

  @BeforeEach
  public void setUp() {
    ServerMock server = MockBukkit.mock();
    WorldMock world = new WorldMock(Material.AIR, 0);
    this.team = new Team(NamedTextColor.AQUA, new Location(world, 0, 0, 0));
    this.player = server.addPlayer("Player");
  }

  @AfterEach
  public void tearDown() {
    MockBukkit.unmock();
  }

  @Test
  void getName() {
    Assertions.assertEquals(team.getName(), NamedTextColor.AQUA.toString());
  }

  @Test
  void size() {
    Assertions.assertEquals(team.size(), 0);
    team.add(player);
    Assertions.assertEquals(team.size(), 1);
  }

  @Test
  void isEmpty() {
    Assertions.assertTrue(team.isEmpty());
    team.add(player);
    Assertions.assertFalse(team.isEmpty());
  }

  @Test
  void contains() {
    Assertions.assertFalse(team.contains(player));
    team.add(player);
    Assertions.assertTrue(team.contains(player));
  }

  @Test
  void iterator() {
    Iterator<Player> iterator = team.iterator();
    Assertions.assertFalse(iterator.hasNext());
    team.add(player);
    iterator = team.iterator();
    Assertions.assertTrue(iterator.hasNext());
    Assertions.assertEquals(iterator.next(), player);
    Assertions.assertFalse(iterator.hasNext());
  }

  @Test
  void toArray() {
    Assertions.assertArrayEquals(team.toArray(), new Player[0]);
    team.add(player);
    Player[] players = new Player[1];
    players[0] = player;
    Assertions.assertArrayEquals(team.toArray(), players);
  }

  @Test
  void testToArray() {
    Player[] tmp = new Player[0];
    Assertions.assertArrayEquals(team.toArray(tmp), new Player[0]);
    team.add(player);
    tmp = new Player[1];
    Player[] players = new Player[1];
    players[0] = player;
    Assertions.assertArrayEquals(team.toArray(tmp), players);
  }

  @Test
  void add() {
    Assertions.assertTrue(team.add(player));
    Assertions.assertFalse(team.add(player));
  }

  @Test
  void remove() {
    Assertions.assertFalse(team.remove(player));
    team.add(player);
    Assertions.assertTrue(team.remove(player));
    Assertions.assertFalse(team.remove(player));
  }

  @Test
  void containsAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> team.containsAll(new HashSet<Player>()));
  }

  @Test
  void addAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> team.addAll(new HashSet<>()));
  }

  @Test
  void retainAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> team.retainAll(new HashSet<Player>()));
  }

  @Test
  void removeAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> team.removeAll(new HashSet<Player>()));
  }

  @Test
  void clear() {
    Assertions.assertThrows(UnsupportedOperationException.class, () -> team.clear());
  }

  @Test
  void getColor() {
    Assertions.assertEquals(team.getColor(), NamedTextColor.AQUA);
  }
}
