package fr.evenus.vortex_example.team;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import fr.evenus.vortex_example.Config;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TeamManagerTest {
  private TeamManager teamManager;
  private PlayerMock player;
  @BeforeEach
  void setUp() throws InvalidConfigurationException {
    ServerMock server = MockBukkit.mock();
    server.addSimpleWorld("world");
    YamlConfiguration tmp = new YamlConfiguration();
    tmp.loadFromString("""
            teams:
              - color: red
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
    this.teamManager = new TeamManager(new Config(tmp, server.getLogger()),4);
    this.player = server.addPlayer("Player");
  }

  @AfterEach
  void tearDown() {
    MockBukkit.unmock();
  }

  @Test
  void size() {
    Assertions.assertEquals(teamManager.size(), 0);
    teamManager.add(player);
    Assertions.assertEquals(teamManager.size(), 1);
  }

  @Test
  void isEmpty() {
    Assertions.assertTrue(teamManager.isEmpty());
    teamManager.add(player);
    Assertions.assertFalse(teamManager.isEmpty());
  }

  @Test
  void contains() {
    Assertions.assertFalse(teamManager.contains(player));
    teamManager.add(player);
    Assertions.assertTrue(teamManager.contains(player));
  }

  @Test
  void iterator() {
    Iterator<Player> iterator = teamManager.iterator();
    Assertions.assertFalse(iterator.hasNext());
    teamManager.add(player);
    iterator = teamManager.iterator();
    Assertions.assertTrue(iterator.hasNext());
    Assertions.assertEquals(iterator.next(), player);
    Assertions.assertFalse(iterator.hasNext());
    Assertions.assertThrows(NoSuchElementException.class, iterator::next);
  }

  @Test
  void toArray() {
    Assertions.assertThrows(UnsupportedOperationException.class, teamManager::toArray);
  }

  @Test
  void testToArray() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> teamManager.toArray(new Player[0]));
  }

  @Test
  void add() {
    Assertions.assertTrue(teamManager.add(player));
    Assertions.assertFalse(teamManager.add(player));
  }

  @Test
  void remove() {
    Assertions.assertFalse(teamManager.remove(player));
    teamManager.add(player);
    Assertions.assertTrue(teamManager.remove(player));
    Assertions.assertFalse(teamManager.remove(player));
  }

  @Test
  void containsAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> teamManager.containsAll(new HashSet<Player>()));
  }

  @Test
  void addAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> teamManager.addAll(new HashSet<>()));
  }

  @Test
  void retainAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> teamManager.retainAll(new HashSet<Player>()));
  }

  @Test
  void removeAll() {
    Assertions.assertThrows(
        UnsupportedOperationException.class, () -> teamManager.removeAll(new HashSet<Player>()));
  }

  @Test
  void clear() {
    Assertions.assertThrows(UnsupportedOperationException.class, teamManager::clear);
  }

  @Test
  void getTeamColor() {
    Assertions.assertNull(teamManager.getTeamColor(player));
    teamManager.add(player);
    Assertions.assertNull(teamManager.getTeamColor(player));
  }

  @Test
  void getTeam() {
    Assertions.assertNull(teamManager.getTeam(player));
    teamManager.add(player);
    Assertions.assertNull(teamManager.getTeam(player));
  }
}
