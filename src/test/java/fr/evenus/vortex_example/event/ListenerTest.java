package fr.evenus.vortex_example.event;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import fr.evenus.vortex_example.Config;
import fr.evenus.vortex_example.game.GameManager;
import fr.evenus.vortex_example.team.TeamManager;
import net.kyori.adventure.text.Component;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.player.PlayerJoinEvent;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ListenerTest {
  private PlayerMock player;
  private Listener listener;
  @BeforeEach
  void setUp() throws InvalidConfigurationException {
    ServerMock server = MockBukkit.mock();
    server.addSimpleWorld("world");
    YamlConfiguration tmp = new YamlConfiguration();
        tmp.loadFromString("""
            teams:
              - color: red
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
        listener = new Listener(new GameManager(new TeamManager(new Config(tmp, server.getLogger()),4)));
        this.player = server.addPlayer("Player");
  }

  @AfterEach
  void tearDown() {
        MockBukkit.unmock();
  }
  @Test
  void onPlayerJoin() {
        PlayerJoinEvent event = new PlayerJoinEvent(player, Component.empty());
        listener.onPlayerJoin(event);
        Assertions.assertEquals(event.joinMessage(),
            Component.text(
                "You are not in a team yet, please join one by using /vortex join <team>"));
  }
}
