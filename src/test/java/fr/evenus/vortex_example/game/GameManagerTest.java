package fr.evenus.vortex_example.game;

import static org.junit.jupiter.api.Assertions.*;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import fr.evenus.vortex_example.Config;
import fr.evenus.vortex_example.team.TeamManager;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GameManagerTest {
  private PlayerMock player;
  private GameManager gameManager;
  @BeforeEach
  void setUp() throws InvalidConfigurationException {
    ServerMock server = MockBukkit.mock();
    server.addSimpleWorld("world");
    YamlConfiguration tmp = new YamlConfiguration();
        tmp.loadFromString("""
            teams:
              - color: red
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
        this.gameManager = new GameManager(new TeamManager(new Config(tmp, server.getLogger()),4));
        this.player = server.addPlayer("Player");
  }

  @AfterEach
  void tearDown() {
        MockBukkit.unmock();
  }
  @Test
  void playerJoin() {
        Assertions.assertNull(gameManager.playerJoin(player));
        Assertions.assertNull(gameManager.playerJoin(player)); // Rejoin
  }
}
