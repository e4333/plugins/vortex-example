package fr.evenus.vortex_example;

import static org.junit.jupiter.api.Assertions.*;

import be.seeseemelk.mockbukkit.MockBukkit;
import be.seeseemelk.mockbukkit.ServerMock;
import be.seeseemelk.mockbukkit.entity.PlayerMock;
import fr.evenus.vortex_example.team.TeamManager;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class VortexPluginTest {
  private VortexPlugin plugin;
  private PlayerMock player;
  @BeforeEach
  void setUp() throws InvalidConfigurationException {
    ServerMock server = MockBukkit.mock();
    server.addSimpleWorld("world");
    this.plugin = MockBukkit.load(VortexPlugin.class);
    this.player = server.addPlayer("Player");
  }

  @AfterEach
  void tearDown() {
    MockBukkit.unmock();
  }

  @Test
  void onEnable() {}

  @Test
  void getCustomConfig() throws InvalidConfigurationException {
    YamlConfiguration tmp = new YamlConfiguration();
        tmp.loadFromString("""
            teams:
              - color: red
                spawnpoint:
                  x: 0
                  y: 0
                  z: 0
                  world: world
            """);
        Config config = new Config(tmp, plugin.getLogger());
        // Assertions.assertEquals(plugin.getCustomConfig(), config);
  }
}
