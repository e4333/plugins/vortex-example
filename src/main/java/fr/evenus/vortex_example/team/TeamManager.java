package fr.evenus.vortex_example.team;

import fr.evenus.vortex_example.Config;
import fr.evenus.vortex_example.VortexPlugin;
import java.util.*;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class TeamManager implements Set<Player> {
  private final Map<String, Team> teams;
  private final Set<Player> noTeam;

  private final int team_size;

  private int size;

  public TeamManager(Config config, int team_size) {
    this.team_size = team_size;
    this.size = 0;

    this.noTeam = new HashSet<>();

    this.teams = new HashMap<>();

    for (Team team : config.getTeams()) {
      this.teams.put(team.getName(), team);
    }
  }

  @Override
  public int size() {
    return this.size;
  }

  @Override
  public boolean isEmpty() {
    return this.size == 0;
  }

  @Override
  public boolean contains(Object o) {
    if (o instanceof Player player) {
      if (this.noTeam.contains(player))
        return true;
      for (Team team : this.teams.values())
        if (team.contains(player))
          return true;
      return false;
    }

    return false;
  }

  @NotNull
  @Override
  public Iterator<Player> iterator() {
    return new Iterator<>() {
      private Iterator<Team> currentTeam = null;
      private Iterator<Player> currentIterator = noTeam.iterator();
      @Override
      public boolean hasNext() {
        if (currentIterator.hasNext())
          return true;
        if (currentTeam == null) {
          currentTeam = teams.values().iterator();
        }
        while (!currentIterator.hasNext() && currentTeam.hasNext()) {
          currentIterator = currentTeam.next().iterator();
        }
        return currentIterator.hasNext();
      }

      @Override
      public Player next() {
        if (currentIterator.hasNext())
          return currentIterator.next();
        if (currentTeam == null) {
          currentTeam = teams.values().iterator();
        }
        while (!currentIterator.hasNext() && currentTeam.hasNext()) {
          currentIterator = currentTeam.next().iterator();
        }
        return currentIterator.next();
      }
    };
  }

  @NotNull
  @Override
  public Object @NotNull[] toArray() {
    throw new UnsupportedOperationException();
  }

  @NotNull
  @Override
  public <T> T @NotNull[] toArray(@NotNull T @NotNull[] ts) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean add(Player player) {
    if (this.contains(player))
      return false;
    if (this.noTeam.add(player)) {
      this.size++;
      return true;
    }
    return false;
  }

  @Override
  public boolean remove(Object o) {
    if (!this.contains(o))
      return false;
    if (this.noTeam.remove(o)) {
      this.size--;
      return true;
    }
    for (Team team : this.teams.values())
      if (team.remove(o)) {
        this.size--;
        return true;
      }
    return false;
  }

  @Override
  public boolean containsAll(@NotNull Collection<?> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(@NotNull Collection<? extends Player> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(@NotNull Collection<?> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeAll(@NotNull Collection<?> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }

  public NamedTextColor getTeamColor(Player player) {
    if (noTeam.contains(player))
      return null;
    for (Team team : teams.values()) {
      if (team.contains(player))
        return team.getColor();
    }
    return null;
  }

  public Team getTeam(Player player) {
    if (noTeam.contains(player))
      return null;
    for (Team team : teams.values()) {
      if (team.contains(player))
        return team;
    }
    return null;
  }
}
