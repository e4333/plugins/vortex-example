package fr.evenus.vortex_example.team;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class Team implements Set<Player> {
  private Set<Player> players;
  private final NamedTextColor color;
  private final Location spawnpoint;

  public Team(NamedTextColor color, Location spawnpoint) {
    this.players = new HashSet<>();

    this.color = color;
    this.spawnpoint = spawnpoint;
  }

  public String getName() {
    return this.color.toString();
  }

  @Override
  public int size() {
    return this.players.size();
  }

  @Override
  public boolean isEmpty() {
    return this.players.isEmpty();
  }

  @Override
  public boolean contains(Object o) {
    return this.players.contains(o);
  }

  @NotNull
  @Override
  public Iterator<Player> iterator() {
    return this.players.iterator();
  }

  @NotNull
  @Override
  public Object @NotNull[] toArray() {
    return this.players.toArray();
  }

  @NotNull
  @Override
  public <T> T @NotNull[] toArray(T @NotNull[] ts) {
    return this.players.toArray(ts);
  }

  @Override
  public boolean add(Player player) {
    return this.players.add(player);
  }

  @Override
  public boolean remove(Object o) {
    return this.players.remove(o);
  }

  @Override
  public boolean containsAll(@NotNull Collection<?> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean addAll(@NotNull Collection<? extends Player> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean retainAll(@NotNull Collection<?> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public boolean removeAll(@NotNull Collection<?> collection) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void clear() {
    throw new UnsupportedOperationException();
  }

  public NamedTextColor getColor() {
    return this.color;
  }
}
