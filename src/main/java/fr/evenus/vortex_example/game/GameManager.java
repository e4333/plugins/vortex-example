package fr.evenus.vortex_example.game;

import fr.evenus.vortex_example.team.Team;
import fr.evenus.vortex_example.team.TeamManager;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class GameManager {
  private enum Status {
    WAITING,
    IN_GAME,
    FINISHED,
  }

  private final TeamManager teamManager;

  private Status status;

  public GameManager(TeamManager teamManager) {
    this.teamManager = teamManager;
    this.status = Status.WAITING;
  }

  public Team playerJoin(Player player) {
    player.getInventory().clear();
    switch (this.status) {
      case WAITING -> {
        player.setGameMode(GameMode.ADVENTURE);
        if (this.teamManager.contains(player)) {
          NamedTextColor color = this.teamManager.getTeamColor(player);
          if (color != null){
            String name = player.getName();
            player.displayName(Component.text(name, color));
            player.playerListName(Component.text(name, color));
          }
        } else {
          this.teamManager.add(player);
        }
      }
      case IN_GAME -> player.setGameMode(GameMode.SPECTATOR);

      case FINISHED -> player.setGameMode(GameMode.ADVENTURE);
      default -> {
        throw new RuntimeException();
      }
    }
    return this.teamManager.getTeam(player);
  }
}
