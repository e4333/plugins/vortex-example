/*
 * @Author: Ultraxime
 * @Date:   2023-11-11 19:11:50
 * @Last Modified by:   Ultraxime
 * @Last Modified time: 2023-11-11 19:16:22
 */

// This file is part of Vortex Example.
//
// Vortex Example is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, either version 3 of the License, or any later version.
//
// Vortex Example is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty
// of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vortex Example. If not, see <https://www.gnu.org/licenses/>.

package fr.evenus.vortex_example;

import fr.evenus.vortex_example.event.Listener;
import fr.evenus.vortex_example.game.GameManager;
import fr.evenus.vortex_example.team.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public class VortexPlugin extends JavaPlugin {
  private Config config;

  @Override
  public void onEnable() {
    Bukkit.getLogger().info("Loading");
    super.onEnable();
    this.config = new Config(this.getConfig(), this.getLogger());
    TeamManager teamManager = new TeamManager(this.getCustomConfig(), 4);
    GameManager gameManager = new GameManager(teamManager);
    this.getServer().getPluginManager().registerEvents(new Listener(gameManager), this);
    Bukkit.getLogger().info("Started");
  }

  @NotNull
  public Config getCustomConfig() {
    return this.config;
  }
}
