package fr.evenus.vortex_example.event;

import fr.evenus.vortex_example.VortexPlugin;
import fr.evenus.vortex_example.game.GameManager;
import fr.evenus.vortex_example.team.Team;
import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class Listener implements org.bukkit.event.Listener {
  private final GameManager gameManager;

  public Listener(GameManager gameManager) {
    this.gameManager = gameManager;
  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();

    Team team = this.gameManager.playerJoin(player);
    if (team != null)
      event.joinMessage(Component.text("You joined the team : " + team.getName()));
    else
      event.joinMessage(Component.text(
          "You are not in a team yet, please join one by using /vortex join <team>"));
  }
}
