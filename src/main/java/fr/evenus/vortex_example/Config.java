package fr.evenus.vortex_example;

import fr.evenus.vortex_example.team.Team;
import java.util.*;
import java.util.logging.Logger;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
  private final FileConfiguration config;
  private final Logger logger;

  public Config(FileConfiguration _config, Logger _logger) {
    this.config = _config;
    this.logger = _logger;
  }
  public Collection<Team> getTeams() {
    List<?> teams = this.config.getList("teams");

    if (teams == null)
      return Collections.emptyList();

    LinkedList<Team> result = new LinkedList<>();

    for (var tmp : teams) {
      try {
        HashMap<String, Object> team = (HashMap<String, Object>) tmp;
        NamedTextColor color = NamedTextColor.NAMES.value(team.get("color").toString());
        if (color == null) {
          logger.warning("The color " + team.get("color") + " is not a valid color.");
          continue;
        }
        try {
          Location spawnpoint =
              Config.getLocation((HashMap<String, Object>) team.get("spawnpoint"));
          result.addFirst(new Team(color, spawnpoint));
        } catch (ClassCastException | NullPointerException err) {
          logger.warning("The spawnpoint of team " + color
              + " is not of proper shape : " + team.get("spawnpoint"));
          logger.warning(String.valueOf(err));
        }
      } catch (ClassCastException | NullPointerException err) {
        logger.warning("A team in the config files is not of the expected format : " + tmp);
        logger.warning(String.valueOf(err));
      }
    }

    return result;
  }

  private static Location getLocation(HashMap<String, Object> location)
      throws IllegalArgumentException {
    World world =
        Bukkit.getWorld(Objects.requireNonNull(location.getOrDefault("world", "world").toString()));
    double x = Double.parseDouble(location.getOrDefault("x", 0).toString());
    double y = Double.parseDouble(location.getOrDefault("y", 0).toString());
    double z = Double.parseDouble(location.getOrDefault("z", 0).toString());
    float yaw = Float.parseFloat(location.getOrDefault("yaw", 0).toString());
    float pitch = Float.parseFloat(location.getOrDefault("pitch", 0).toString());
    return new Location(world, x, y, z, yaw, pitch);
  }
}
